﻿using DAL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DAO
{
    public class SalaryDAO : EmployeeContext
    {
        public static List<MONTH> GetMonths()
        {
            return db.MONTHs.ToList();
        }

        public static void AddSalary(SALARY salary)
        {
            try
            {
                db.SALARies.InsertOnSubmit(salary);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static List<SalaryDetailDTO> GetSalaries()
        {
            List<SalaryDetailDTO> salarylist = new List<SalaryDetailDTO>();
            var list = (from s in db.SALARies
                        join e in db.EMPLOYEEs on s.Employee equals e.ID
                        join m in db.MONTHs on s.MonthID equals m.ID
                        select new
                        {
                            UserNo = e.UserNo,
                            Name = e.Name,
                            Surname = e.Surname,
                            EmployeeID = s.Employee,
                            Amount = s.Amount,
                            Year = s.Year,
                            MonthName = m.MonthName,
                            MonthID = s.MonthID,
                            SalaryID = s.ID,
                            DepartmentID = e.DepartmentID,
                            PositionID = e.Position
                        }).OrderBy(x=>x.Year).ToList();
            foreach (var item in list)
            {
                SalaryDetailDTO dto = new SalaryDetailDTO();
                dto.UserNo = item.UserNo;
                dto.Name = item.Name;
                dto.Surname = item.Surname;
                dto.EmployeeID = (int)item.EmployeeID;
                dto.SalaryAmount = (int)item.Amount;
                dto.SalaryYear = (int)item.Year;
                dto.MonthName = item.MonthName;
                dto.MonthID = (int)item.MonthID;
                dto.SalaryID = item.SalaryID;
                dto.DepartmentID = item.DepartmentID;
                dto.PositionID = item.PositionID;
                dto.OldSalary = (int)item.Amount;
                salarylist.Add(dto);
            }

            return salarylist;
        }

        public static void DeleteSalary(int salaryID)
        {
            try
            {
                SALARY sal = db.SALARies.First(x => x.ID == salaryID);
                db.SALARies.DeleteOnSubmit(sal);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static void UpdateSalary(SALARY salary, bool control)
        {
            try
            {
                SALARY sl = db.SALARies.First(x => x.ID == salary.ID);
                sl.Amount = salary.Amount;
                sl.Year = salary.Year;
                sl.MonthID = salary.MonthID;
                db.SubmitChanges();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
