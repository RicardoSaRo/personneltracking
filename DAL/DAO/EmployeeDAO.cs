﻿using DAL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DAO
{
    public class EmployeeDAO : EmployeeContext
    {
        public static void AddEmployee(EMPLOYEE employee)
        {
            try
            {
                db.EMPLOYEEs.InsertOnSubmit(employee);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<EmployeeDetailedDTO> GetEmployees()
        {
            List<EmployeeDetailedDTO> employeeList = new List<EmployeeDetailedDTO>();
            var list = (from e in db.EMPLOYEEs
                        join d in db.DEPARTMENTs on e.DepartmentID equals d.ID
                        join p in db.POSITIONs on e.Position equals p.ID
                        select new
                        {
                            UserNo = e.UserNo,
                            Name = e.Name,
                            Surname = e.Surname,
                            EmployeeID = e.ID,
                            Password = e.Password,
                            DepartmentName = d.DepartmentName,
                            DepartmentID = d.ID,
                            PositionName = p.PositionName,
                            Position = p.ID,
                            isAdmin = e.isAdmin,
                            Salary = e.Salary,
                            ImagePath = e.ImagePath,
                            Birthday = e.BirthDay,
                            Address = e.Address,
                        }).OrderBy(x => x.UserNo).ToList();

            foreach (var item in list)
            {
                EmployeeDetailedDTO dto = new EmployeeDetailedDTO();
                dto.UserNo = item.UserNo;
                dto.Name = item.Name;
                dto.Surname = item.Surname;
                dto.EmployeeID = item.EmployeeID;
                dto.Password = item.Password;
                dto.DepartmentName = item.DepartmentName;
                dto.DepartmentID = item.DepartmentID;
                dto.PositionName = item.PositionName;
                dto.PositionID = item.Position;
                dto.isAdmin = item.isAdmin;
                dto.Salary = item.Salary;
                dto.ImagePath = item.ImagePath;
                dto.Birthday = item.Birthday;
                dto.Address = item.Address;
                employeeList.Add(dto);
            }

            return employeeList;
        }

        public static void DeleteEmployee(int employeeID)
        {
            try
            {
                EMPLOYEE employ = db.EMPLOYEEs.First(x => x.ID == employeeID);
                db.EMPLOYEEs.DeleteOnSubmit(employ);
                db.SubmitChanges();
                /*List<TASK> tlist = db.TASKs.Where(x => x.EmployeeID == employeeID).ToList();
                db.TASKs.DeleteAllOnSubmit(tlist);
                db.SubmitChanges();
                List<SALARY> slist = db.SALARies.Where(x => x.Employee == employeeID).ToList();
                db.SALARies.DeleteAllOnSubmit(slist);
                db.SubmitChanges();
                List<PERMISSION> plist = db.PERMISSIONs.Where(x => x.EmployeeID == employeeID).ToList();
                db.PERMISSIONs.DeleteAllOnSubmit(plist);
                db.SubmitChanges();*/
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateEmployee(POSITION position)
        {
            try
            {
                List<EMPLOYEE> emp = db.EMPLOYEEs.Where(x => x.Position == position.ID).ToList();
                foreach (var item in emp)
                {
                    item.DepartmentID = position.DepartmentID;
                }
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateEmployee(EMPLOYEE empl)
        {
            try
            {
                EMPLOYEE employee = db.EMPLOYEEs.First(x => x.ID == empl.ID);
                employee.UserNo = empl.UserNo;
                employee.Name = empl.Name;
                employee.Surname = empl.Surname;
                employee.Password = empl.Password;
                employee.isAdmin = empl.isAdmin;
                employee.Salary = empl.Salary;
                employee.ImagePath = empl.ImagePath;
                employee.BirthDay = empl.BirthDay;
                employee.Address = empl.Address;
                employee.DepartmentID = empl.DepartmentID;
                employee.Position = empl.Position;
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateEmployee(int empl, int amount)
        {
            try
            {
                EMPLOYEE employee = db.EMPLOYEEs.First(x => x.ID == empl);
                employee.Salary = amount;
                db.SubmitChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<EMPLOYEE> GetEmployees(int v1, string text)
        {
            try
            {
                List<EMPLOYEE> list = db.EMPLOYEEs.Where(x => x.UserNo == v1 && x.Password == text).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<EMPLOYEE> GetUsers(int v)
        {
            return db.EMPLOYEEs.Where(x => x.UserNo == v).ToList();
        }
    }
}
