﻿using DAL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DAO
{
    public class TaskDAO : EmployeeContext
    {
        public static List<TASKSTATE> GetTaskStates()
        {
            return db.TASKSTATEs.ToList();
        }

        public static void AddTask(TASK task)
        {
            try
            {
                db.TASKs.InsertOnSubmit(task);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static List<TaskDetailedDTO> GetTasks()
        {
            List<TaskDetailedDTO> tasklist = new List<TaskDetailedDTO>();

            var list = (from t in db.TASKs
                        join s in db.TASKSTATEs on t.TaskState equals s.ID
                        join e in db.EMPLOYEEs on t.EmployeeID equals e.ID
                        join d in db.DEPARTMENTs on e.DepartmentID equals d.ID
                        join p in db.POSITIONs on e.Position equals p.ID
                        select new
                        {
                            taskID = t.ID,
                            title = t.TaskTitle,
                            content=t.TaskContent,
                            startdate=t.TaskStartDate,
                            deliverydate=t.TaskDeliverDate,
                            taskStateNames=s.StateName,
                            taskStateID=t.TaskState,
                            UserNo=e.UserNo,
                            Name=e.Name,
                            EmployeeID=t.EmployeeID,
                            Surname=e.Surname,
                            Position=p.PositionName,
                            Department=d.DepartmentName,
                            PositionID=e.Position,
                            DepartmentID=e.DepartmentID,
                        }).OrderBy(x=>x.startdate).ToList();
            foreach (var item in list)
            {
                TaskDetailedDTO dto = new TaskDetailedDTO();
                dto.TaskID = item.taskID;
                dto.Title = item.title;
                dto.Content = item.content;
                dto.TaskStartDate = item.startdate;
                dto.TaskDeliverDate = item.deliverydate;
                dto.TaskStateName = item.taskStateNames;
                dto.TaskStateID = item.taskStateID;
                dto.UserNo = item.UserNo;
                dto.Name = item.Name;
                dto.Surname = item.Surname;
                dto.DepartmentName = item.Department;
                dto.PositionName = item.Position;
                dto.PositionID = item.PositionID;
                dto.EmployeeID = item.EmployeeID;
                tasklist.Add(dto);
            }
            return tasklist;
        }

        public static void ApproveTask(int taskId, bool isAdmin)
        {
            try
            {
                TASK tk = db.TASKs.First(x => x.ID == taskId);
                if (isAdmin)
                    tk.TaskState = TaskStates.Approved;
                else
                    tk.TaskState = TaskStates.Delivered;
                tk.TaskDeliverDate = DateTime.Today;
                db.SubmitChanges();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static void DeleteTask(int taskID)
        {
            try
            {
                TASK tsk = db.TASKs.First(x => x.ID == taskID);
                db.TASKs.DeleteOnSubmit(tsk);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static void UpdateTask(TASK task)
        {
            try
            {
                TASK ts = db.TASKs.First(x => x.ID == task.ID);
                ts.TaskTitle = task.TaskTitle;
                ts.TaskContent = task.TaskContent;
                ts.TaskState = task.TaskState;
                ts.EmployeeID = task.EmployeeID;
                db.SubmitChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
