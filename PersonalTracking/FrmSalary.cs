﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using BLL;
using DAL.DTO;

namespace PersonalTracking
{
    public partial class FrmSalary : Form
    {
        public FrmSalary()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
        SalaryDTO dto = new SalaryDTO();
        bool comboFull = false;
        public SalaryDetailDTO detail = new SalaryDetailDTO();
        public bool isUpdate = false;

        private void FrmSalary_Load(object sender, EventArgs e)
        {
            dto = SalaryBLL.GetAll();
            if (!isUpdate)
            {
                dataGridView1.DataSource = dto.Employees;
                dataGridView1.Columns[0].Visible = false;
                dataGridView1.Columns[1].HeaderText = "UserNo";
                dataGridView1.Columns[2].HeaderText = "Name";
                dataGridView1.Columns[3].HeaderText = "Surname";
                dataGridView1.Columns[4].Visible = false;
                dataGridView1.Columns[5].HeaderText = "Department";
                dataGridView1.Columns[6].Visible = false;
                dataGridView1.Columns[7].HeaderText = "Position";
                dataGridView1.Columns[8].Visible = false;
                dataGridView1.Columns[9].Visible = false;
                dataGridView1.Columns[10].Visible = false;
                dataGridView1.Columns[11].Visible = false;
                dataGridView1.Columns[12].Visible = false;
                dataGridView1.Columns[13].Visible = false;
                //dataGridView1.Columns[14].Visible = false;
                comboFull = false;
                cmbDepartment.DataSource = dto.Departments;
                cmbDepartment.DisplayMember = "DepartmentName";
                cmbDepartment.ValueMember = "ID";
                cmbPosition.DataSource = dto.Positions;
                cmbPosition.DisplayMember = "Position";
                cmbPosition.ValueMember = "ID";
                
                
                cmbPosition.SelectedValue = -1;
                cmbDepartment.SelectedValue = -1;
                
                
                comboFull = true;
            }
            cmbMonth.DisplayMember = "MonthName";
            cmbMonth.ValueMember = "ID";
            cmbMonth.DataSource = dto.Months;
            cmbMonth.SelectedValue = -1;
            if (isUpdate)
            {
                panel1.Hide();
                txtName.Text = detail.Name;
                txtSurname.Text = detail.Surname;
                txtSalary.Text = detail.SalaryAmount.ToString();
                txtYear.Text = detail.SalaryYear.ToString();
                cmbMonth.SelectedValue = detail.MonthID;
            }
            
        }

        SALARY salary = new SALARY();
        int oldsalary = 0;

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtYear.Text.Trim() == "")
                MessageBox.Show("Please fill the year");
            else if (cmbMonth.SelectedIndex == -1)
                MessageBox.Show("Please fill the month");
/*            else if (cmbDepartment.SelectedIndex == -1)
                MessageBox.Show("Please select a department");
            else if (cmbPosition.SelectedIndex == -1)
                MessageBox.Show("Please select a position");   */
            else if (txtSalary.Text.Trim() == "")
                MessageBox.Show("Please fill the salary");
            else
            {
                bool control = false;
                if (!isUpdate)
                {
                    if (salary.Employee == 0)
                        MessageBox.Show("Please select an employee from table");
                    else
                    {
                        salary.Year = Convert.ToInt32(txtYear.Text);
                        salary.MonthID = Convert.ToInt32(cmbMonth.SelectedValue);
                        salary.Amount = Convert.ToInt32(txtSalary.Text);
                        if (salary.Amount > oldsalary)
                            control = true;
                        SalaryBLL.AddSalary(salary,control);
                        MessageBox.Show("Salary added successfully");
                        salary = new SALARY();
                        cmbMonth.SelectedIndex = -1;
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("Are you sure?", "title", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (DialogResult.Yes == result)
                    {
                        SALARY salary = new SALARY();
                        salary.ID = detail.SalaryID;
                        salary.Amount = Convert.ToInt32(txtSalary.Text);
                        salary.Year = Convert.ToInt32(txtYear.Text);
                        salary.MonthID = cmbMonth.SelectedIndex+1;
                        salary.Employee = detail.EmployeeID;
                        if (salary.Amount > detail.OldSalary)
                            control = true;
                        MessageBox.Show(salary.ID.ToString() +" "+ salary.Amount.ToString() +" "+ salary.Year.ToString() +" "+ salary.MonthID.ToString() +" "+ salary.Employee.ToString() +" "+ control.ToString());
                        SalaryBLL.UpdateSalary(salary, control);
                        MessageBox.Show("Salary Updated");
                        this.Close();
                    }
                }
            }
            
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            txtUserNo.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtName.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtSurname.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            txtYear.Text = DateTime.Today.Year.ToString();
            txtSalary.Text = dataGridView1.Rows[e.RowIndex].Cells[8].Value.ToString();
            salary.Employee = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value);
            oldsalary = Convert.ToInt32((dataGridView1.Rows[e.RowIndex].Cells[8].Value).ToString());

        }
    }
}
