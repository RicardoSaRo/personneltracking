﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DAL;
using DAL.DTO;

namespace PersonalTracking
{
    public partial class FrmPermission : Form
    {
        public FrmPermission()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        TimeSpan PermissionDay;
        public bool isUpdate = false;
        public PermissionDetailDTO detail = new PermissionDetailDTO();

        private void FrmPermission_Load(object sender, EventArgs e)
        {
            txtUserNo.Text = UserStatic.UserNo.ToString();
            if (isUpdate)
            {
                dtpStart.Value = (DateTime)detail.StartDate;
                dtpEnd.Value = (DateTime)detail.EndDate;
                txtDateAmount.Text = detail.PermissionDayAmount.ToString();
                txtExplanation.Text = detail.Explanation.ToString();
                txtUserNo.Text = detail.UserNo.ToString();
            }
        }

        private void dtpStart_ValueChanged(object sender, EventArgs e)
        {
            PermissionDay = dtpEnd.Value.Date - dtpStart.Value.Date;
            txtDateAmount.Text = PermissionDay.TotalDays.ToString();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtDateAmount.Text.Trim() == "")
                MessageBox.Show("Please change End or Start Date.");
            else if (Convert.ToInt32(txtDateAmount.Text) <= 0)
                MessageBox.Show("Permission days must be bigger than cero.");
            else if (txtExplanation.Text.Trim() == "")
                MessageBox.Show("Please fill an explanation for the permission.");
            else
            {
                PERMISSION Permission = new PERMISSION();
                if (!isUpdate)
                {
                    Permission.EmployeeID = UserStatic.EmployeeID;
                    Permission.PermissionState = 1;
                    Permission.PermissionStartDate = dtpStart.Value.Date;
                    Permission.PermissionEndDate = dtpEnd.Value.Date;
                    Permission.PermissionExplanation = txtExplanation.Text;
                    Permission.PermissionDay = Convert.ToInt32(txtDateAmount.Text);
                    PermissionBLL.AddPermission(Permission);
                    MessageBox.Show("New Permission Added");
                    dtpEnd.Value = DateTime.Today;
                    dtpStart.Value = DateTime.Today;
                    txtDateAmount.Clear();
                    txtExplanation.Clear();
                }
                else if (isUpdate)
                {
                    DialogResult result = MessageBox.Show("Are you sure?", "Warning", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        Permission.ID = detail.PermissionID;
                        Permission.PermissionExplanation = txtExplanation.Text;
                        Permission.PermissionStartDate = dtpStart.Value;
                        Permission.PermissionEndDate = dtpEnd.Value;
                        Permission.PermissionDay = Convert.ToInt32(txtDateAmount.Text);
                        PermissionBLL.UpdatePermission(Permission);
                        MessageBox.Show("Permission was updated");
                        this.Close();
                    }
                }
            }
        }
    }
}
