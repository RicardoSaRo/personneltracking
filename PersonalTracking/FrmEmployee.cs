﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DAL;
using DAL.DTO;
using System.IO;

namespace PersonalTracking
{
    public partial class FrmEmployee : Form
    {
        public FrmEmployee()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtUserNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = General.isNumber(e);
        }

        private void txtSalary_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSalary_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = General.isNumber(e);
        }

        EmployeeDTO dto = new EmployeeDTO();
        public EmployeeDetailedDTO detailed = new EmployeeDetailedDTO();
        public bool isUpdate = false;
        string imagepath = "";

        private void FrmEmployee_Load(object sender, EventArgs e)
        {
            dto = EmployeeBLL.GetAll();
            cmbDepartment.DataSource = dto.Departments;
            cmbDepartment.DisplayMember = "DepartmentName";
            cmbDepartment.ValueMember = "ID";
            cmbPosition.DataSource = dto.Positions;
            cmbPosition.DisplayMember = "Position";
            cmbPosition.ValueMember = "ID";
            cmbPosition.SelectedValue = -1;
            cmbDepartment.SelectedValue = -1;
            comboFull = true;
            if (isUpdate)
            {
                txtName.Text = detailed.Name;
                txtSurname.Text = detailed.Surname;
                txtSalary.Text = detailed.Salary.ToString();
                cmbDepartment.SelectedValue = detailed.DepartmentID;
                cmbPosition.SelectedValue = detailed.PositionID;
                dtpBirthday.Value = detailed.Birthday;
                txtAddress.Text = detailed.Address;
                chAdmin.Checked = Convert.ToBoolean(detailed.isAdmin);
                txtPassword.Text = detailed.Password;
                txtUserNo.Text = detailed.UserNo.ToString();
                imagepath = Application.StartupPath + "\\images\\" + detailed.ImagePath;
                txtImagePath.Text = imagepath;
                pictureBox1.ImageLocation = imagepath;
                if(!UserStatic.isAdmin)
                {
                    chAdmin.Enabled = false;
                    txtUserNo.Enabled = false;
                    txtSalary.Enabled = false;
                    cmbDepartment.Enabled = false;
                    cmbPosition.Enabled = false;
                    btnCheck.Hide();
                }
            }
        }

        bool comboFull = false;
        private void cmbDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboFull)
            {
                int departmentID = Convert.ToInt32(cmbDepartment.SelectedValue);
                cmbPosition.DataSource = dto.Positions.Where(x => x.DepartmentID == departmentID).ToList();
            }
        }

        string fileName = "";
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Load(openFileDialog1.FileName);
                txtImagePath.Text = openFileDialog1.FileName;
                string Unique = Guid.NewGuid().ToString();
                fileName += Unique + openFileDialog1.SafeFileName;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtUserNo.Text.Trim() == "")
                MessageBox.Show("User No is Empty");
            else if (!EmployeeBLL.isUnique(Convert.ToInt32(txtUserNo.Text)))
                MessageBox.Show("This User No is used by another employee. Please use another one.");
            else if (txtPassword.Text.Trim() == "")
                MessageBox.Show("Password is Empty");
            else if (txtName.Text.Trim() == "")
                MessageBox.Show("Name is Empty");
            else if (txtSurname.Text.Trim() == "")
                MessageBox.Show("Surname is Empty");
            else if (txtSalary.Text.Trim() == "")
                MessageBox.Show("Salary is Empty");
            else if (cmbDepartment.SelectedIndex == -1)
                MessageBox.Show("Department is Empty");
            else if (cmbPosition.SelectedIndex == -1)
                MessageBox.Show("Position is Empty");
            else 
            {
                EMPLOYEE employee = new EMPLOYEE();
                if (!isUpdate)
                {
                    if (!EmployeeBLL.isUnique(Convert.ToInt32(txtUserNo.Text)))
                        MessageBox.Show("This User No is used by another employee. Please use another one.");
                    else
                    {


                        employee.UserNo = Convert.ToInt32(txtUserNo.Text);
                        employee.Password = txtPassword.Text;
                        employee.isAdmin = chAdmin.Checked;
                        employee.Name = txtName.Text;
                        employee.Surname = txtSurname.Text;
                        employee.Salary = Convert.ToInt32(txtSalary.Text);
                        employee.DepartmentID = Convert.ToInt32(cmbDepartment.SelectedValue);
                        employee.Position = Convert.ToInt32(cmbPosition.SelectedValue);
                        employee.Address = txtAddress.Text;
                        employee.BirthDay = dtpBirthday.Value;
                        employee.ImagePath = fileName;
                        EmployeeBLL.AddEmployee(employee);
                        File.Copy(txtImagePath.Text, @"images\\" + fileName);
                        MessageBox.Show("Employee was added");
                        txtUserNo.Clear();
                        txtPassword.Clear();
                        chAdmin.Checked = false;
                        txtName.Clear();
                        txtSurname.Clear();
                        txtSalary.Clear();
                        comboFull = false;
                        cmbDepartment.SelectedValue = -1;
                        cmbPosition.DataSource = dto.Positions;
                        cmbPosition.SelectedValue = -1;
                        comboFull = true;
                        txtAddress.Clear();
                        dtpBirthday.Value = DateTime.Today;
                        txtImagePath.Clear();
                        pictureBox1.Image = null;
                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show("Are you sure?","Warning",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
                    if (DialogResult.Yes == result)
                    {
                        EMPLOYEE empl = new EMPLOYEE();
                        if (txtImagePath.Text!=imagepath)
                        {
                            if (File.Exists(@"images\\" + detailed.ImagePath))
                                File.Exists(@"images\\" + detailed.ImagePath);
                            File.Copy(txtImagePath.Text, @"images\\" + fileName);
                            empl.ImagePath = fileName;
                        }
                        else
                            empl.ImagePath = imagepath;
                        empl.ID = detailed.EmployeeID;
                        empl.UserNo = Convert.ToInt32(txtUserNo.Text);
                        empl.Name = txtName.Text;
                        empl.Surname = txtSurname.Text;
                        empl.isAdmin = chAdmin.Checked;
                        empl.Password = txtPassword.Text;
                        empl.Address = txtAddress.Text;
                        empl.BirthDay = Convert.ToDateTime(dtpBirthday.Value);
                        empl.DepartmentID = Convert.ToInt32(cmbDepartment.SelectedValue);
                        empl.Position = Convert.ToInt32(cmbPosition.SelectedValue);
                        empl.Salary = Convert.ToInt32(txtSalary.Text);
                        EmployeeBLL.UpdateEmployee(empl);
                    }
                }
            }
        }

        bool isUnique = false;
        private void btnCheck_Click(object sender, EventArgs e)
        {
            if (txtUserNo.Text.Trim() == "")
                MessageBox.Show("User No is Empty");
            else 
            {
                isUnique = EmployeeBLL.isUnique(Convert.ToInt32(txtUserNo.Text));
                if (!isUnique)
                    MessageBox.Show("This User No is used by another employee. Please use another one.");
                else
                    MessageBox.Show("This User No is usable.");
            }
        }
    }
}
